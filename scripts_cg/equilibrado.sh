gmx grompp -f ../sirah.ff/tutorial/6/GPU/eq1_CGLIPROT.mdp -p ../topol.top -n ../2kyv_DMPC_cg_ion.ndx -c 2kyv_DMPC_cg_em2.gro -r 2kyv_DMPC_cg_em2.gro -o 2kyv_DMPC_cg_eq1.tpr -maxwarn 1
gmx mdrun -deffnm 2kyv_DMPC_cg_eq1 &> EQ1.log
gmx grompp -f ../sirah.ff/tutorial/6/GPU/eq2_CGLIPROT.mdp -p ../topol.top -n ../2kyv_DMPC_cg_ion.ndx -c 2kyv_DMPC_cg_eq1.gro -o 2kyv_DMPC_cg_eq2.tpr
gmx mdrun -deffnm 2kyv_DMPC_cg_eq2 &> EQ2.log

