gmx grompp -f ../sirah.ff/tutorial/6/GPU/md_CGLIPROT.mdp -p ../topol.top -n ../2kyv_DMPC_cg_ion.ndx -c 2kyv_DMPC_cg_eq1.gro -o 2kyv_DMPC_cg_md.tpr -maxwarn 1
gmx mdrun -deffnm 2kyv_DMPC_cg_md &> MD.log
