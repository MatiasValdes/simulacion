gmx grompp -f ../sirah.ff/tutorial/6/GPU/em1_CGLIPROT.mdp -p ../topol.top -n ../2kyv_DMPC_cg_ion.ndx -c ../2kyv_DMPC_cg_ion.gro -r ../2kyv_DMPC_cg_ion.gro -o 2kyv_DMPC_cg_em1.tpr -maxwarn 1
gmx mdrun -deffnm 2kyv_DMPC_cg_em1 &> EM1.log 
gmx grompp -f ../sirah.ff/tutorial/6/GPU/em2_CGLIPROT.mdp -p ../topol.top -n ../2kyv_DMPC_cg_ion.ndx -c 2kyv_DMPC_cg_em1.gro -r 2kyv_DMPC_cg_em1.gro -o 2kyv_DMPC_cg_em2.tpr -maxwarn 1
gmx mdrun -deffnm 2kyv_DMPC_cg_em2 &> EM2.log 

