import pandas as pd
import matplotlib.pyplot as plt

data_5ns = pd.read_csv('/home/matias/Documentos/SMII/Lab7/DMclasica_abf_Z/Graf_Cys_ABF/output/holasistema_final.2.pmf', delim_whitespace=True, skiprows=1, header=None, names=['xi', 'A(xi)'])
data_10ns = pd.read_csv('/home/matias/Documentos/SMII/Lab7/DMclasica_abf_Z/Graf_Cys/pmf_sistema_final.dat', delim_whitespace=True, skiprows=1, header=None, names=['xi', 'A(xi)'])
#data_100ns = pd.read_csv('sistema_final.2.pmf', delim_whitespace=True, skiprows=1, header=None, names=['xi', 'A(xi)'])

plt.figure(figsize=(10,6))
#plt.xticks(range(-180, 181, 30))


plt.plot(data_5ns['xi'], data_5ns['A(xi)'], label='ABF', color='red')
plt.plot(data_10ns['xi'], data_10ns['A(xi)'], label='DM clásica', color='blue')
#plt.plot(data_100ns['xi'], data_100ns['A(xi)'], label='100ns', color='green')


plt.xlabel('Distancia (A)')
plt.ylabel('Energia libre (kcal/mol)')
plt.title('Comparación entre PMF de dinámica clásica y ABF')
plt.legend()
plt.grid(True)


plt.show()

