for sys in sistema_final
do	
	# vmd -dispdev text -e calColvars.tcl -args nameOut colvarsFile colvar psf namdConfiguration stride dir/ @dcd
	vmd -dispdev text -e /home/matias/scripts/calcColvars.tcl -args traj_colvars_prod_${sys} dihedral.colvars Dihed $sys.psf namd/prod.namd 1 ./ output/${sys}.2.dcd 
	
	/home/matias/HistoDistro/histoDistro Dihed_traj_colvars_prod_${sys}.dat 100 hist_${sys}
	
	kT=0.5961573
	awk -v kT=$kT $2>0 '{print $1, -kT*log($2)}' hist_${sys} > pmf_${sys}.dat

done

# PLOT 
python3 ../plot_pmf.py
